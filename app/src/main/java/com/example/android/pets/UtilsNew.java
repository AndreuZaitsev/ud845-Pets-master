package com.example.android.pets;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class UtilsNew {
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_PET_ID = "pet_id";
    private static final String KEY_PET_NAME = "pet_name";
    private static final String KEY_PET_PRICE = "pet_price";
    private static final String KEY_PET_BREED = "pet_breed";
    private static final String KEY_PET_GENDER = "pet_gender";
    private static final String KEY_PET_IMAGE = "pet_image";
    public static String petGender = "";

    private static final String LOG_TAG = UtilsNew.class.getSimpleName();

    private UtilsNew() {
    }

    public static List<Pets> fetchPetsData(String requestUrl) throws IOException {
        Log.i(LOG_TAG, "TEST: fetchPetsData()");

        //create URL object
        URL url = createUrl(requestUrl);

        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        List<Pets> pets = extractFeatureFromJson(jsonResponse);
        return pets;


    }

    private static List<Pets> extractFeatureFromJson(String JSON) throws IOException {
        if (JSON.isEmpty()) {
            return null;
        }
        List<Pets> pets = new ArrayList<>();

        Bitmap thumbnailBitmap;
        Log.i(LOG_TAG, "TESTING: extractFeatureFromJson() ");

        try {
            JSONObject baseJsonResponse = new JSONObject(JSON);
            int success = baseJsonResponse.getInt(KEY_SUCCESS);

            if (success == 1) {
                JSONArray petsArray = baseJsonResponse.getJSONArray(KEY_DATA);
                for (int i = 0; i < petsArray.length(); i++) {
                    JSONObject pet = petsArray.getJSONObject(i);

                    Integer petId = pet.getInt(KEY_PET_ID);
                    String petName = pet.getString(KEY_PET_NAME);
                    Integer petPrice = pet.getInt(KEY_PET_PRICE);
                    String petBreed = pet.getString(KEY_PET_BREED);
                    Integer petGenderInt = pet.getInt(KEY_PET_GENDER);
                    String petImage = pet.getString(KEY_PET_IMAGE);
                    URL url = new URL(petImage);
                    thumbnailBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                    switch (petGenderInt) {
                        case 0:
                            petGender = "Unknown";
                            break;
                        case 1:
                            petGender = "Male";
                            break;
                        case 2:
                            petGender = "Female";
                            break;
                        default:
                            petGender = "Unknown";
                    }

                    pets.add(new Pets(thumbnailBitmap, petName, petPrice.toString(), petBreed, petGender, petId.toString()));
                }
                return pets;
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing the news JSON results", e);
        }
        return null;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL", e);
            return null;
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponce = "";
        if (url == null) {
            return jsonResponce;
        }
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponce = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error responce code" + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            // TODO: Handle the exception
            Log.e(LOG_TAG, "Problems retrieving the book JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponce;
    }

    private static String readFromStream(InputStream inputStream) throws IOException{
        StringBuilder output = new StringBuilder();
        if (inputStream != null){
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null){
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }
}
