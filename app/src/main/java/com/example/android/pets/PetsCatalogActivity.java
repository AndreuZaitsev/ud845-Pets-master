package com.example.android.pets;

import android.annotation.SuppressLint;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.pets.helper.CheckNetworkStatus;
import com.example.android.pets.helper.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Displays list of pets that were entered and stored in the app.
 */
public class PetsCatalogActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Pets>>{
    public static final String LOG_TAG = PetsCatalogActivity.class.getSimpleName();

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_PET_ID = "pet_id";
    private static final String KEY_PET_NAME = "pet_name";
    private static final String KEY_PET_PRICE = "pet_price";
    private static final String KEY_PET_BREED = "pet_breed";
    private static final String KEY_PET_GENDER = "pet_gender";
    private static final String KEY_PET_IMAGE = "pet_image";

    private String petImage;
    private ImageView imageView;
    private Bitmap bitmap;

    private static final int NEWS_LOADER_ID = 1;
    private LoaderManager loaderManager ;

    public String petGender = "";
    ListView petsListView;
    PetsAdapter adapter;
    private static final String BASE_URL = "http://zaytssss.000webhostapp.com/";

    private ArrayList<HashMap<String,String>> petList;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PetsCatalogActivity.this, PetEditorActivity.class);
                startActivity(intent);
            }
        });

        // Find the ListView which will be populated with the pet data
        petsListView = (ListView) findViewById(android.R.id.list);

        // Find the swipeRefreshLayout
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        petsListView.setEmptyView(emptyView);

        // Set adapter fol petListView
        adapter = new PetsAdapter(this, new ArrayList<Pets>());
        petsListView.setAdapter(adapter);




        /**
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        myUpdateOperation();
                    }
                }
        );


        petsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    String petId = ((TextView) view.findViewById(R.id.petId)).getText().toString();

                    // Create new intent to go to {@link PetEditorActivity}
                    Intent intent = new Intent(PetsCatalogActivity.this,PetEditorActivity.class);
                    intent.putExtra(KEY_PET_ID, petId);
                    startActivityForResult(intent, 20);

                } else {
                    Toast.makeText(PetsCatalogActivity.this,
                            getString(R.string.check_internet_connection),
                            Toast.LENGTH_LONG).show();

                }
            }
        });

        loaderManager = getLoaderManager();
        loaderManager.initLoader(NEWS_LOADER_ID, null, this);
        // Kick off the AsyncTask
//        new FetchPets().execute();
    }

    private void myUpdateOperation() {
        loaderManager = getLoaderManager();
        loaderManager.restartLoader(NEWS_LOADER_ID, null, PetsCatalogActivity.this);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();



        // Kick off the AsyncTask to refresh the list of pets.
//        new FetchPets().execute();
    }

    /** Fetches the list of pets from the server */
    @SuppressLint("StaticFieldLeak")
    private class FetchPets extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Display progress bar
            progressDialog = new ProgressDialog(PetsCatalogActivity.this);
            progressDialog.setMessage(getString(R.string.loading_pets));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Utils utils = new Utils();
            JSONObject jsonObject = utils.makeHtpRequest(
                    BASE_URL + getString(R.string.url_fetch_all_pets),
                    "GET",
                    null);
            Log.i(LOG_TAG, "TEST: jsonObj" + jsonObject.toString());
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray pets;
                if (success == 1){
                    petList = new ArrayList<>();
                    pets = jsonObject.getJSONArray(KEY_DATA);
                    for (int i = 0; i < pets.length(); i++){
                        JSONObject pet = pets.getJSONObject(i);

                        Integer petId = pet.getInt(KEY_PET_ID);
                        String petName = pet.getString(KEY_PET_NAME);
                        Integer petPrice = pet.getInt(KEY_PET_PRICE);
                        String petBreed = pet.getString(KEY_PET_BREED);
                        Integer petGenderInt = pet.getInt(KEY_PET_GENDER);
                        petImage = pet.getString(KEY_PET_IMAGE);

                        switch (petGenderInt){
                            case 0:
                                petGender = getString(R.string.gender_unknown);
                                break;
                            case 1:
                                petGender = getString(R.string.gender_male);
                                break;
                            case 2:
                                petGender = getString(R.string.gender_female);
                                break;
                            default:
                                petGender = getString(R.string.gender_unknown);
                        }

                        HashMap<String,String> map = new HashMap<>();
                        map.put(KEY_PET_ID, petId.toString());
                        map.put(KEY_PET_NAME, petName);
                        map.put(KEY_PET_PRICE, petPrice.toString());
                        map.put(KEY_PET_BREED, petBreed);
                        map.put(KEY_PET_GENDER, petGender);
                        petList.add(map);
                        }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            URL url = null;
            try {
                url = new URL(BASE_URL + petImage);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateList();
                }
            });

        }
    }

    /** Get bitmap of image and convert it to base64 string */
    public String getStringImage(Bitmap bmp)  {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);

//        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        String encodedImage = "";
        try {
            encodedImage = URLEncoder.encode(Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.i(LOG_TAG,"TEST: encodedImage= " +encodedImage);
        return encodedImage;
    }
    /** Updating parsed json data into the ListView */
    private void populateList() {

        adapter = new PetsAdapter(PetsCatalogActivity.this, new ArrayList<Pets>());
        petsListView.setAdapter(adapter);

//        ListAdapter adapter = new SimpleAdapter(
//                PetsCatalogActivity.this,
//                petList,
//                R.layout.list_item,
//                new String[]{KEY_PET_ID, KEY_PET_NAME, KEY_PET_PRICE, KEY_PET_BREED, KEY_PET_GENDER, getStringImage(bitmap)},
//                new int[]{R.id.petId, R.id.petName, R.id.petPrice, R.id.petBreed, R.id.petGender, R.id.imageView});
//
//        // Updating ListView.
//        petListView.setAdapter(adapter);

        // Setup the item clickListener.
        Log.i(LOG_TAG, "TEST: adapter petID= ");
        // Setup the item click listener
        petsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    String petId = ((TextView) view.findViewById(R.id.petId)).getText().toString();

                    // Create new intent to go to {@link PetEditorActivity}
                    Intent intent = new Intent(PetsCatalogActivity.this,PetEditorActivity.class);
                    intent.putExtra(KEY_PET_ID, petId);
                    startActivityForResult(intent, 20);

                } else {
                    Toast.makeText(PetsCatalogActivity.this,
                            getString(R.string.check_internet_connection),
                            Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert dummy data" menu option
            case R.id.action_insert_dummy_data:
                new Toast(PetsCatalogActivity.this).makeText(PetsCatalogActivity.this,"actionAdd", Toast.LENGTH_LONG).show();
                return true;
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_entries:
                new Toast(PetsCatalogActivity.this).makeText(PetsCatalogActivity.this,"actionDel", Toast.LENGTH_LONG).show();
                return true;
            case R.id.menu_refresh:
                // Signal SwipeRefreshLayout to start the progress indicator
                swipeRefreshLayout.setRefreshing(true);

                // Start the refresh background task.
                // This method calls setRefreshing(false) when it's finished.
                myUpdateOperation();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 20){
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }


    public Loader<List<Pets>> onCreateLoader(int i, Bundle bundle) {
        Log.i(LOG_TAG, "TEST: onCreateLoader");
        // ProgressBar showing fetching all pets.
        progressDialog = new ProgressDialog(PetsCatalogActivity.this);
        progressDialog.setMessage(getString(R.string.loading_pets));
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        Uri baseUri = Uri.parse(BASE_URL + getString(R.string.url_fetch_all_pets));
        Uri.Builder uriBuilder = baseUri.buildUpon();

        Log.i(LOG_TAG, "TEST: uriBuilder = "+uriBuilder.toString());
        return new PetsLoader(this,uriBuilder.toString());

    }

    public void onLoadFinished(Loader<List<Pets>> loader, List<Pets> pets) {
        Log.i(LOG_TAG, "TEST: onLoadFinished ");
        adapter.clear();
        if (pets != null && !pets.isEmpty()) {
            adapter.addAll(pets);
        }
        swipeRefreshLayout.setRefreshing(false);
        progressDialog.dismiss();
    }

    public void onLoaderReset(Loader<List<Pets>> loader) {
        Log.i(LOG_TAG, "TEST: onLoaderReset ");
        adapter.clear();
    }

    //end
}
