package com.example.android.pets;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class MailActivity extends AppCompatActivity {

    public static final String LOG_TAG = MailActivity.class.getSimpleName();
    private static final String KEY_PET_NAME = "pet_name";
    private static final String KEY_PET_PRICE = "pet_price";
    private static final String KEY_PET_BREED = "pet_breed";
    private static final String KEY_PET_GENDER = "pet_gender";

    private String summaryMessage;

    private EditText mPetNameEditText;
    private EditText mBreedEditText;
    private EditText mPriceEditText;
    private Spinner mGenderSpinner;
    private EditText mSupplierNameEditText;
    private EditText mSupplierEmailEditText;
    private EditText mSenderNameEditText;
    private EditText mSubjectEditText;
    private EditText mOptionalMessageEditText;

    private String petName;
    private String petPrice;
    private String petBreed;
    private String petGender;
    private String petGenderString;
    private String supplierName;
    private String supplierEmails;
    private String senderName;
    private String subject;
    private String optionalMessage;

    private int mGender = 0;

    private boolean mPetHasChanged = false;

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mPetHasChanged = true;
            return false;
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mail_activity);

        setTitle(getString(R.string.category_mail));


        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMail);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();

            }
        });

        Intent intent = getIntent();
        petName = intent.getStringExtra(KEY_PET_NAME);
        petPrice = intent.getStringExtra(KEY_PET_PRICE);
        petBreed = intent.getStringExtra(KEY_PET_BREED);
        petGender = intent.getStringExtra(KEY_PET_GENDER);
        String petGender2 = petGender;
        mGender = Integer.parseInt(petGender2);

        // Find all relevant views that we will need to read user input from
        mPetNameEditText = (EditText) findViewById(R.id.mail_edit_pet_name);
        mBreedEditText = (EditText) findViewById(R.id.mail_edit_pet_breed);
        mPriceEditText = (EditText) findViewById(R.id.mail_edit_pet_price);
        mGenderSpinner = (Spinner) findViewById(R.id.mail_spinner_gender);

        mSupplierNameEditText = (EditText) findViewById(R.id.edit_supplier_name);
        mSupplierEmailEditText = (EditText) findViewById(R.id.edit_supplier_email);
        mSenderNameEditText = (EditText) findViewById(R.id.edit_mail_sender);
        mSubjectEditText = (EditText) findViewById(R.id.edit_mail_subject);
        mOptionalMessageEditText = (EditText) findViewById(R.id.edit_mail_message);

        // Populate pet's data into editTexts
        mPetNameEditText.setText(petName);
        mBreedEditText.setText(petBreed);
        mPriceEditText.setText(petPrice);

        // Setup OnTouchListeners on all the input fields, so we can determine if the user
        // has touched or modified them. This will let us know if there are unsaved changes
        // or not, if the user tries to leave the editor without saving.
        mPetNameEditText.setOnTouchListener(mTouchListener);
        mBreedEditText.setOnTouchListener(mTouchListener);
        mPriceEditText.setOnTouchListener(mTouchListener);
        mGenderSpinner.setOnTouchListener(mTouchListener);

        setupSpinner();
        mGenderSpinner.setSelection(mGender);



    }

    /** Function sending message to Supplier via another emailApp*/
    @SuppressLint("StringFormatInvalid")
    private void sendMessage(){
        // Get inputs from editTexts.
        petName = mPetNameEditText.getText().toString().trim();
        petPrice = mPriceEditText.getText().toString().trim();
        petBreed = mBreedEditText.getText().toString().trim();
        supplierName = mSupplierNameEditText.getText().toString().trim();
        senderName = mSenderNameEditText.getText().toString().trim();
        subject = mSubjectEditText.getText().toString().trim();
        optionalMessage = mOptionalMessageEditText.getText().toString().trim();
        if (!optionalMessage.equals("")){
            optionalMessage = "\n" + optionalMessage + "\n";
//            optionalMessage += "\n";

        }
        // Create an array of recipients emails.
        supplierEmails = mSupplierEmailEditText.getText().toString().trim();
        String[] recipients = supplierEmails.split(",");

        // Change numbers to full name of genders.
        switch (petGender) {
            case "0":
                petGenderString = getString(R.string.gender_unknown);
                break;
            case "1":
                petGenderString = getString(R.string.gender_male);
                break;
            default:
                petGenderString = getString(R.string.gender_female);
                break;
        }

        // Create a full message for sending.
        summaryMessage = getString(R.string.msg_hello, supplierName)
                + "\n" + "\n" + getString(R.string.msg_start_message)
                + "\n" + "\n" + getString(R.string.msg_pet_name, petName)
                + "\n" +        getString(R.string.msg_pet_price, petPrice)
                + "\n" +        getString(R.string.msg_pet_gender, petGenderString)
                + "\n" +        optionalMessage
                + "\n" +        getString(R.string.msg_end_message, senderName);

        // Create intent for sending message via email apps.
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, summaryMessage);
        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent, getString(R.string.choose_email_app)));
    }

    /** Setup the dropdown spinner that allows the user to select the gender of the pet */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mGenderSpinner.setAdapter(genderSpinnerAdapter);

        // Set the integer mSelected to the constant values
        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_male))) {
                        mGender = 1;
                        petGender = "1";
                    } else if (selection.equals(getString(R.string.gender_female))) {
                        mGender = 2;
                        petGender = "2";
                    } else {
                        mGender = 0;
                        petGender = "0";
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = 0;
                petGender = "0";
            }
        });
    }

    /**  This method is called when the back button is pressed. */
    @Override
    public void onBackPressed() {
        // If the pet hasn't changed, continue with handling back button press
        if (!mPetHasChanged) {
            super.onBackPressed();
            return;
        }

        // Otherwise if there are unsaved changes, setup a dialog to warn the user.
        // Create a click listener to handle the user confirming that changes should be discarded.
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // User clicked "Discard" button, close the current activity.
                        finish();
                    }
                };

        // Show dialog that there are unsaved changes
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    /**
     * Show a dialog that warns the user there are unsaved changes that will be lost
     * if they continue leaving the editor.
     *
     * @param discardButtonClickListener is the click listener for what to do when
     *                                   the user confirms they want to discard their changes
     */
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    //end
}
