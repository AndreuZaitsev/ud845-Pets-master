
package com.example.android.pets;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.pets.helper.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Allows user to create a new pet or edit an existing one.
 */
public class PetEditorActivity extends AppCompatActivity {
    public static final String LOG_TAG = PetEditorActivity.class.getSimpleName();

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_PET_ID = "pet_id";
    private static final String KEY_PET_NAME = "pet_name";
    private static final String KEY_PET_PRICE = "pet_price";
    private static final String KEY_PET_BREED = "pet_breed";
    private static final String KEY_PET_GENDER = "pet_gender";
    private static final String KEY_PET_IMAGE = "pet_image";
    private static final String BASE_URL = "http://zaytssss.000webhostapp.com/";


    private EditText mNameEditText;
    private EditText mBreedEditText;
    private EditText mPriceEditText;
    private Spinner mGenderSpinner;

    private ImageView imageView;
    private FloatingActionButton fab;

    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private Uri filePath;

    private String petName;
    private String petImage;
    private String petPrice;
    private String petBreed;
    private String petGender;
    private int success;
    private ProgressDialog progressDialog;

    /** Check for the existing pet (null if it's a new pet) */
    private String petId;

      /** Gender of the pet. */
    private int mGender = 0;

    /** Boolean flag that keeps track of whether the pet has been edited (true) or not (false) */
    private boolean mPetHasChanged = false;

    /**
     * OnTouchListener that listens for any user touches on a View, implying that they are modifying
     * the view, and we change the mPetHasChanged boolean to true.
     */
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mPetHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        // Examine the intent that was used to launch this activity,
        // in order to figure out if we're creating a new pet or editing an existing one.
        Intent intent = getIntent();

        // Get pet ID from CatalogActivity.
        petId = intent.getStringExtra(KEY_PET_ID);

        // If the intent DOES NOT contain a pet content URI, then we know that we are
        // creating a new pet.
        if (petId == null) {
            // This is a new pet, so change the app bar to say "Add a Pet"
            setTitle(getString(R.string.editor_activity_title_new_pet));

            // Invalidate the options menu, so the "Delete" menu option can be hidden.
            // (It doesn't make sense to delete a pet that hasn't been created yet.)
            invalidateOptionsMenu();
        } else {
            // Otherwise this is an existing pet, so change app bar to say "Edit Pet"
            setTitle(getString(R.string.editor_activity_title_edit_pet));

            // We need to Update / Delete a pet.
            new FetchPetDetails().execute();
        }

        // Find all relevant views that we will need to read user input from
        mNameEditText = (EditText) findViewById(R.id.edit_pet_name);
        mBreedEditText = (EditText) findViewById(R.id.edit_pet_breed);
        mPriceEditText = (EditText) findViewById(R.id.edit_pet_price);
        mGenderSpinner = (Spinner) findViewById(R.id.spinner_gender);
        imageView = (ImageView) findViewById(R.id.image_upload);

        fab = (FloatingActionButton) findViewById(R.id.fabImage);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        // Setup OnTouchListeners on all the input fields, so we can determine if the user
        // has touched or modified them. This will let us know if there are unsaved changes
        // or not, if the user tries to leave the editor without saving.
        mNameEditText.setOnTouchListener(mTouchListener);
        mBreedEditText.setOnTouchListener(mTouchListener);
        mPriceEditText.setOnTouchListener(mTouchListener);
        mGenderSpinner.setOnTouchListener(mTouchListener);

        setupSpinner();
    }


    /** Get bitmap of image and convert it to base64 string */
    public String getStringImage(Bitmap bmp)  {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);

//        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        String encodedImage = "";
        try {
            encodedImage = URLEncoder.encode(Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.i(LOG_TAG,"TEST: encodedImage= " +encodedImage);
        return encodedImage;
    }

    /** Choose an image from the gallery. */
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    /** Convert image to bitmap and set into the imageView */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();

            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(filePath));
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** Setup the dropdown spinner that allows the user to select the gender of the pet */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mGenderSpinner.setAdapter(genderSpinnerAdapter);

        // Set the integer mSelected to the constant values
        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_male))) {
                        mGender = 1;
                        petGender = "1";
                    } else if (selection.equals(getString(R.string.gender_female))) {
                        mGender = 2;
                        petGender = "2";
                    } else {
                        mGender = 0;
                        petGender = "0";
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = 0;
                petGender = "0";
            }
        });
    }

    /** Get user input from editor and save pet into database.*/
    private void savePet() {
        // Read from input fields
        // Use trim to eliminate leading or trailing white space
         petName = mNameEditText.getText().toString().trim();
         petBreed = mBreedEditText.getText().toString().trim();
         petPrice = mPriceEditText.getText().toString().trim();

        // Check if this is supposed to be a new pet
        // and check if all the fields in the editor are blank
        if (petId == null &&
                TextUtils.isEmpty(petName) && TextUtils.isEmpty(petBreed) &&
                TextUtils.isEmpty(petPrice) && mGender == 0) {
            // Since no fields were modified, we can return early without creating a new pet.
            // No need to create ContentValues and no need to do any ContentProvider operations.
            return;
        }




        // Determine if this is a new or existing pet by checking if mCurrentPetUri is null or not
        if (petId == null) {
            // This is a NEW pet, so insert a new pet ,

            new SavePetAsyncTask().execute();

        } else {
            // Otherwise this is an EXISTING pet, so update the pet with content URI: mCurrentPetUri
            // and pass in the new ContentValues. Pass in null for the selection and selection args
            // because mCurrentPetUri will already identify the correct row in the database that
            // we want to modify.
            new UpdatePetAsyncTask().execute();

        }
    }

    /**
     * AsyncTask for updating a pet.
     * */
    @SuppressLint("StaticFieldLeak")
    private class UpdatePetAsyncTask extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            progressDialog = new ProgressDialog(PetEditorActivity.this);
            progressDialog.setMessage(getString(R.string.updating_pet_please_wait));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Utils utils = new Utils();
            Map<String, String> mapHttpParams = new HashMap<>();

            Log.i(LOG_TAG,"TESTing: petId = "+petId);
            Log.i(LOG_TAG,"TESTing: petName = "+petName);
            Log.i(LOG_TAG,"TESTing: petBreed = "+petBreed);
            Log.i(LOG_TAG,"TESTing: petPrice = "+petPrice);
            Log.i(LOG_TAG,"TESTing: petGender = "+petGender);
            Log.i(LOG_TAG,"TESTing: petGender = "+petGender);

            // Populating request parameters.
            mapHttpParams.put(KEY_PET_ID, petId);
            mapHttpParams.put(KEY_PET_NAME, petName);
            mapHttpParams.put(KEY_PET_BREED, petBreed);
            mapHttpParams.put(KEY_PET_PRICE, petPrice);
            mapHttpParams.put(KEY_PET_GENDER, petGender);
            JSONObject jsonObject = utils.makeHtpRequest(
                    BASE_URL + getString(R.string.url_update_pet),
                    "POST",
                    mapHttpParams);
            try {
                Log.i(LOG_TAG,"TESTing: jsonObject = "+jsonObject);

                success = jsonObject.getInt(KEY_SUCCESS);
                Log.i(LOG_TAG,"TESTing: success = "+success);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (success == 1){
                        // Display success message.
                        Toast.makeText(PetEditorActivity.this,
                                getString(R.string.editor_update_pet_successful), Toast.LENGTH_LONG).show();

                        Intent intent = getIntent();
                        //send result code 20 to notify about movie update
                        setResult(20, intent);
                        //Finish this activity and go back to listing activity
                        finish();
                    } else {
                        Toast.makeText(PetEditorActivity.this,
                                getString(R.string.editor_update_pet_failed),
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }

    /**
     *  AsyncTask for adding a pet
     *  */
    @SuppressLint("StaticFieldLeak")
    private class SavePetAsyncTask extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            progressDialog = new ProgressDialog(PetEditorActivity.this);
            progressDialog.setMessage(getString(R.string.adding_pet_please_wait));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i(LOG_TAG,"TEST= loading");
            Utils utils = new Utils();
            Map<String, String> mapHttpParams = new HashMap<>();
            // Populating request parameters.
            mapHttpParams.put(KEY_PET_NAME, petName);
            mapHttpParams.put(KEY_PET_BREED, petBreed);
            mapHttpParams.put(KEY_PET_PRICE, petPrice);
            mapHttpParams.put(KEY_PET_GENDER, petGender);

            JSONObject jsonObject = utils.makeHtpRequest(
                    BASE_URL + getString(R.string.url_add_pet),
                    "POST",
                     mapHttpParams);
            try {
                success = jsonObject.getInt(KEY_SUCCESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   if (success == 1){
                       // Display success message.
                       Toast.makeText(PetEditorActivity.this,
                               getString(R.string.editor_insert_pet_successful), Toast.LENGTH_LONG).show();

                       Intent intent = getIntent();
                       //send result code 20 to notify about movie update
                       setResult(20, intent);
                       //Finish this activity and go back to listing activity
                       finish();
                   } else {
                       Toast.makeText(PetEditorActivity.this,
                               getString(R.string.editor_insert_pet_failed),
                               Toast.LENGTH_LONG).show();
                   }
                }
            });
        }

    }

    /**
     * AsyncTask for fetching details of single pet
     * */
    @SuppressLint("StaticFieldLeak")
    private class FetchPetDetails extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            progressDialog = new ProgressDialog(PetEditorActivity.this);
            progressDialog.setMessage(getString(R.string.loading_pet_details_wait));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Utils utils = new Utils();
            Map<String,String> mapHttpParams = new HashMap<>();
            mapHttpParams.put(KEY_PET_ID, petId);
            JSONObject jsonObject = utils.makeHtpRequest(
                    BASE_URL + getString(R.string.url_pet_details),
                    "GET",
                    mapHttpParams);
            try{
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONObject pet;
                if (success == 1){
                    // Parse the JSON
                    pet = jsonObject.getJSONObject(KEY_DATA);
                    petName = pet.getString(KEY_PET_NAME);
                    Integer petPriceInt = pet.getInt(KEY_PET_PRICE);
                    petBreed = pet.getString(KEY_PET_BREED);
                    mGender = pet.getInt(KEY_PET_GENDER);
                    petPrice = petPriceInt.toString();
                    petImage = pet.getString(KEY_PET_IMAGE);

                    switch (mGender){
                        case 0:
                            petGender = getString(R.string.gender_unknown);
                            break;
                        case 1:
                            petGender = getString(R.string.gender_male);
                            break;
                        case 2:
                            petGender = getString(R.string.gender_female);
                            break;
                        default:
                            petGender = getString(R.string.gender_unknown);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            URL url = null;
            try {
                url = new URL(petImage);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;


        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Populate the EditTexts once network activity is finished executing.
                    mNameEditText.setText(petName);
                    mBreedEditText.setText(petBreed);
                    mPriceEditText.setText(petPrice);
                    mGenderSpinner.setSelection(mGender);

                    imageView.setImageBitmap(bitmap);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    /**
     * This method is called after invalidateOptionsMenu(), so that the
     * menu can be updated (some menu items can be hidden or made visible).
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new pet, hide the "Delete" menu item.
        if (petId == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            MenuItem menuItem2 = menu.findItem(R.id.action_order);
            menuItem2.setVisible(false);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save pet to database
                savePet();

                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                // Pop up confirmation dialog for deletion
                showDeleteConfirmationDialog();
                return true;
            case R.id.action_order:
                // Go to MailActivity
                Intent intent = new Intent(PetEditorActivity.this,MailActivity.class);
                intent.putExtra(KEY_PET_NAME, petName);
                intent.putExtra(KEY_PET_BREED, petBreed);
                intent.putExtra(KEY_PET_GENDER, petGender);
                intent.putExtra(KEY_PET_PRICE, petPrice);

                startActivity(intent);
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // If the pet hasn't changed, continue with navigating up to parent activity
                // which is the {@link CatalogActivity}.
                if (!mPetHasChanged) {
                    NavUtils.navigateUpFromSameTask(PetEditorActivity.this);
                    return true;
                }

                // Otherwise if there are unsaved changes, setup a dialog to warn the user.
                // Create a click listener to handle the user confirming that
                // changes should be discarded.
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User clicked "Discard" button, navigate to parent activity.
                                NavUtils.navigateUpFromSameTask(PetEditorActivity.this);
                            }
                        };

                // Show a dialog that notifies the user they have unsaved changes
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**  This method is called when the back button is pressed. */
    @Override
    public void onBackPressed() {
        // If the pet hasn't changed, continue with handling back button press
        if (!mPetHasChanged) {
            super.onBackPressed();
            return;
        }

        // Otherwise if there are unsaved changes, setup a dialog to warn the user.
        // Create a click listener to handle the user confirming that changes should be discarded.
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // User clicked "Discard" button, close the current activity.
                        finish();
                    }
                };

        // Show dialog that there are unsaved changes
        showUnsavedChangesDialog(discardButtonClickListener);
    }


    /**
     * Show a dialog that warns the user there are unsaved changes that will be lost
     * if they continue leaving the editor.
     *
     * @param discardButtonClickListener is the click listener for what to do when
     *                                   the user confirms they want to discard their changes
     */
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    /** Prompt the user to confirm that they want to delete this pet.  */
    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                new DeletePet().execute();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /** Perform the AsyncTask for deletion of the pet in the database. */
    @SuppressLint("StaticFieldLeak")
    private class DeletePet extends AsyncTask<String,String,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            progressDialog = new ProgressDialog(PetEditorActivity.this);
            progressDialog.setMessage(getString(R.string.deleting_pet_wait));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Utils utils = new Utils();
            Map<String,String> mapHttpParams = new HashMap<>();

            // Set pet ID parameter in request.
            mapHttpParams.put(KEY_PET_ID, petId);
            JSONObject jsonObject = utils.makeHtpRequest(
                    BASE_URL + getString(R.string.url_delete_pet),
                    "POST",
                    mapHttpParams);
            try{
                success = jsonObject.getInt(KEY_SUCCESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (success == 1){
                        // Display success message.
                        Toast.makeText(PetEditorActivity.this,
                                getString(R.string.editor_delete_pet_successful),Toast.LENGTH_LONG).show();
                        Intent intent = getIntent();
                        setResult(20, intent);
                        finish();
                    } else{
                        Toast.makeText(PetEditorActivity.this,
                                getString(R.string.editor_delete_pet_failed),Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

}