package com.example.android.pets;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

public class PetsLoader extends AsyncTaskLoader<List<Pets>>{
    private String url;
    private static final String LOG_TAG = PetsLoader.class.getSimpleName();

    public PetsLoader(Context context, String url) {
        super(context);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        Log.i(LOG_TAG,"TEST: onStartLoading ");
        forceLoad();
    }

    @Override
    public List<Pets> loadInBackground() {
        Log.i(LOG_TAG,"TEST: loadInBackground ");
        List<Pets> pets = null;
        try {
            pets = UtilsNew.fetchPetsData(url);
        } catch (IOException e) {
            Log.e(LOG_TAG,"TEST: loadInBackground " + e);
        }
        return pets;
    }


}
