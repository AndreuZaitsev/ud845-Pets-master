package com.example.android.pets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PetsAdapter extends ArrayAdapter<Pets> {

    private static final String LOG_TAG = PetsCatalogActivity.class.getSimpleName();

    public PetsAdapter(@NonNull Context context,@NonNull List<Pets> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;

        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }
        Pets currentPets = getItem(position);

        TextView nameTextView = (TextView) listItemView.findViewById(R.id.petName);
        nameTextView.setText(currentPets.getName());

        TextView priceTextView = (TextView) listItemView.findViewById(R.id.petPrice);
        priceTextView.setText(currentPets.getPrice());

        TextView breedTextView = (TextView) listItemView.findViewById(R.id.petBreed);
        breedTextView.setText(currentPets.getBreed());

        TextView genderTextView = (TextView) listItemView.findViewById(R.id.petGender);
        genderTextView.setText(currentPets.getGender());

        TextView idTextView = (TextView) listItemView.findViewById(R.id.petId);
        idTextView.setText(currentPets.getId());

        ImageView imageView = (ImageView) listItemView.findViewById(R.id.imageView);
        imageView.setImageBitmap(formatImageFromBitmap(currentPets.getImage()));
        return listItemView;
    }

    // Get the thumbnail image
    private Bitmap formatImageFromBitmap(Bitmap articleThumbnail) {
        // Bitmap for image
        Bitmap returnBitmap;
        // Check thumbnail valid
        if (articleThumbnail == null) {
            // If not valid return default image
            returnBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_empty_shelter);
        } else {
            // If valid return image
            returnBitmap = articleThumbnail;
        }
        // Return bitmap
        return returnBitmap;
    }
    //end
}
