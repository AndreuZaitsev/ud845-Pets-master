package com.example.android.pets;

import android.graphics.Bitmap;

public class Pets {
    private Bitmap image;
    private String name;
    private String price;
    private String breed;
    private String gender;
    private String id;

    public Pets(Bitmap image, String name, String price, String breed, String gender, String id) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.breed = breed;
        this.gender = gender;
        this.id = id;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getBreed() {
        return breed;
    }

    public String getGender() {
        return gender;
    }

    public String getId() {
        return id;
    }
}
